import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import {APP_BASE_HREF} from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { EpisodiosService } from './servicio/episodios.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EpisodiosComponent } from './interface/episodios/episodios.component';
import { RegistroComponent } from './interface/registro/registro.component';
import { LoginComponent } from './interface/login/login.component';
import { LayoutComponent } from './interface/layout/layout.component';


  


@NgModule({
  declarations: [
    AppComponent,
    EpisodiosComponent,
    RegistroComponent,
    LoginComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule

  ],
  providers: [EpisodiosService,{provide: APP_BASE_HREF,useValue:'/'}],
  bootstrap: [AppComponent]
})

export class AppModule { }