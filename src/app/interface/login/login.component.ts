import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { LoginService } from '../../servicio/login.service';
import { UsuarioService } from '../../servicio/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username =  new FormControl('');
  password =  new FormControl('');
  companyId = new FormControl('');
  desdeMs = new FormControl('');

  private log = {
    username:String,
    password:String,
    companyId:String,
    desdeMs:String

  };


  constructor(private loginService:LoginService) { }

  ngOnInit(): void {
  }

  login() {
    this.log.username = this.username.value;
    this.log.password   = this.password.value;
    this.log.companyId = this.companyId.value;
    this.log.desdeMs = this.desdeMs.value;
    
    return this.loginService.loginUsuario(JSON.stringify(this.log)).subscribe(
      response => console.log("Success! ", response),
      error => console.error("Error: ", error)
   );
  }


}
