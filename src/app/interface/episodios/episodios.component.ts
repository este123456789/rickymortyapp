import { Component, OnInit } from '@angular/core';
import { Episodio }  from './episodio';
import { EpisodiosService } from '../../servicio/episodios.service';

@Component({
  selector: 'app-episodios',
  templateUrl: './episodios.component.html',
  styleUrls: ['./episodios.component.scss']
})
export class EpisodiosComponent implements OnInit {

  episodios!: Episodio[];

  constructor(private episodiosService:EpisodiosService) { }

  ngOnInit(): void {
    this.getallEpisodios()
  }

getallEpisodios():void{
  this.episodiosService.getallepisodios().subscribe(
    episodios => {
  this.episodios =  episodios.results
  console.log(this.episodios)
    
    }
    );


}


}
