import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UsuarioService } from '../../servicio/usuario.service';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})

export class RegistroComponent implements OnInit {
  nombre = new FormControl('');
  apellido = new FormControl('');
  docident = new FormControl('');
  email =  new FormControl('');
  clave =  new FormControl('');
  cia =  new FormControl('');

  private usuario = {
    nombre: String,
    apellido:  String,
    docIdent: String,
    email : String,
    clave : String,
    cia : String


  };


  constructor(private usuarioService:UsuarioService) { }

  ngOnInit(): void {
  }
  registrarse(){
    this.usuario.nombre = this.nombre.value;
    this.usuario.apellido = this.apellido.value;
    this.usuario.docIdent = this.docident.value;
    this.usuario.email = this.email.value;
    this.usuario.clave = this.clave.value;
    this.usuario.cia = this.cia.value;

    return this.usuarioService.insertUsuario(JSON.stringify(this.usuario)).subscribe(
      response => console.log("Success! ", response),
      error => console.error("Error: ", error)
   );

  }

}
