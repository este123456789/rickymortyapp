import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EpisodiosComponent } from './interface/episodios/episodios.component';
import { RegistroComponent } from './interface/registro/registro.component';
import { LoginComponent } from './interface/login/login.component';
import { LayoutComponent } from './interface/layout/layout.component';



const routes : Routes = [
  {
    path: '',
    component: LayoutComponent,
    children:  [
      {path:'api',component:LayoutComponent,pathMatch:'full'},
      {path:'api/episode',component:EpisodiosComponent},
      {path:'api/SOL/RegistroInicialSolicitante',component:RegistroComponent},
      {path:'api/SEG',component:LoginComponent}
      
    ]
  },
  {
    path:'**',
    redirectTo:''
  }
  
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
