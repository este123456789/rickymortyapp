import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  
  uriLogin = 'https://pruebas.midasoft.co:5443/Apis_DLLO/Security/'


  constructor(private http:HttpClient) { }


  loginUsuario(loginData:any){
     
    const headers  = new HttpHeaders({'Content-Type':'application/json'});  
    return this.http.post(this.uriLogin, loginData, { headers: headers });


  }


}
