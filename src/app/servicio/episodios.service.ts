import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EpisodiosService {


  uri  = 'https://rickandmortyapi.com/api/episode'

  constructor(private http:HttpClient) { }

  getallepisodios():Observable<any>{
    const headers  = new HttpHeaders({'Content-Type':'application/json'});  	
  	return this.http.get(this.uri,{headers:headers});

  }


}
