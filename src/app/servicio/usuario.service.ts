import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  uriRegistro = 'https://pruebas.midasoft.co:5443/Apis_DLLO/Seleccion/'


  constructor(private http:HttpClient) { }
  insertUsuario(usuarioData:any) {
    
    const headers  = new HttpHeaders({'Content-Type':'application/json'});  
    return this.http.post(this.uriRegistro, usuarioData, { headers: headers });
  }





}
