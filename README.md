# Rickymortyapp
Realizado en  [Angular CLI](https://github.com/angular/angular-cli) version 11.2.2.

## Urls 🚀


http://localhost:4200/api/SOL/RegistroInicialSolicitante

http://localhost:4200/api/SEG

http://localhost:4200/api/episode



# Versionado 📌
»
«
###
1.0.0

### Instalaciòn 📋

- Instalar Nodejs
- Instalar angular cli
- correr comando:  npm i -S
- correr comando: ng serve

## Autor ✒️

Esteban Villa Ramirez. 2021

